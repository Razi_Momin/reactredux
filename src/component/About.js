import React from 'react'
import { connect } from 'react-redux'

const About = (props) => {
    return (
        <div>
            <button onClick={() => { props.changeName('yooooooo') }}>Change it from about page</button>
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        myname: state.name,
        wishes: state.wishes
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        changeName: (name) => {
            dispatch({ type: 'CHANGE_NAME', payload: name })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(About)
