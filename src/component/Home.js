import React from 'react'
import { connect } from 'react-redux'

function Home(props) {
    console.log(props);
    return (
        <div>
            My name is <p>{props.myname}</p>
            <button onClick={() => { props.changeName('momin') }}>Change it</button>
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        myname: state.name,
        wishes: state.wishes
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeName: (name) => {
            dispatch({ type: 'CHANGE_NAME', payload: name })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
