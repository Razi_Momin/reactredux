import React from 'react';
import { connect } from 'react-redux';
import Home from './component/Home';
import About from './component/About';

function App(props) {
  // console.log(props)
  return (
    <div className="App">
      <Home />
      <About />
      {/* My name is <p>{props.myname}</p>
      <button onClick={() => { props.changeName('momin') }}>Change it</button> */}
    </div>
  );
}
// const mapStateToProps = (state) => {
//   return {
//     myname: state.name,
//     wishes: state.wishes
//   }
// }
// const mapDispatchToProps = (dispatch) => {
//   return {
//     changeName: (name) => {
//       dispatch({ type: 'CHANGE_NAME', payload: name })
//     }
//   }
// }
export default App;
