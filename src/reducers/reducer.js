const iState = {
    name: 'Razi',
    wishes: ['eat', 'code']
}

const reducer = (state = iState, action) => {
    switch (action.type) {
        case "CHANGE_NAME":
            return {
                ...state,
                name: action.payload
            }
        default:
            return state;
    }
}
export default reducer;